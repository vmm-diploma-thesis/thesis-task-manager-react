import './App.css';
import TaskNew from './pages/TaskNew.js'
import TaskOverview from './pages/TaskOverview.js'
import {
    BrowserRouter, Routes, Route,
} from "react-router-dom";
import Footer from "./components/Footer";

function App() {
    return (
        <>
            <BrowserRouter>
                <Routes className="page-content">
                    <Route path="" element={<TaskOverview/>}/>
                    <Route path="new" element={<TaskNew/>}/>
                </Routes>
            </BrowserRouter>
            <Footer/>
        </>
    );
}

export default App;
