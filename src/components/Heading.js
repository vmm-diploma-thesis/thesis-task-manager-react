import React from 'react';

function Heading(props) {
    return (
        <div className="d-flex justify-content-center my-4">
            <h1 className="text-center">{props.text}</h1>
        </div>
    );
}

export default Heading;
