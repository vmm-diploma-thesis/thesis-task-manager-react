import React from 'react';

function Footer() {
    return (
        <div className="footer-clean fixed-bottom">
            <footer>
                <p className="copyright">HTL St. Pölten - Data Science © 2022</p>
            </footer>
        </div>

    );
}

export default Footer;
