import React, {useState} from "react";
import {Link} from "react-router-dom";
import {Button, Container, Grid, TextField} from "@mui/material";
import Heading from "../components/Heading";

function TaskNew() {
    document.title = "Task New"
    const [description, setDescription] = useState("");
    const [dateDone, setDateDone] = useState("");
    const [hoursWorked, setHoursWorked] = useState("");
    const [message, setMessage] = useState("");

    let handleSubmit = async (e) => {
        e.preventDefault();
        fetch("http://localhost:8080/api/tasks", {
            method: "POST",
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                description: description,
                dateDone: dateDone,
                hoursWorked: Math.floor(parseInt(hoursWorked))
            })
        }).then(res => {
            if (res.status === 201) {
                setMessage("Task successfully created");
                window.location.href = '/';
            } else {
                setMessage("Some error occurred");
            }
        }).catch(err => {
            console.log(err);
            // Error handling
        });
    };

    return (
        <Container>
            <Heading text="Create a new task"/>
            <form onSubmit={handleSubmit}>
                <Grid container rowSpacing={2} columnSpacing={{md: 3}}>
                    <Grid item xs={6}>
                        <TextField
                            label="Description"
                            variant="outlined"
                            type="text"
                            value={description}
                            required
                            onChange={(e) => setDescription(e.target.value)}
                            fullWidth
                        />
                    </Grid>
                    <Grid item xs={3}>
                        <TextField
                            label="Date done"
                            InputLabelProps={{
                                shrink: true,
                            }}
                            variant="outlined"
                            type="date"
                            value={dateDone}
                            required
                            onChange={(e) => setDateDone(e.target.value)}
                            fullWidth
                        />
                    </Grid>
                    <Grid item xs={3}>
                        <TextField
                            label="Hours Worked"
                            variant="outlined"
                            type="number"
                            value={hoursWorked}
                            required
                            onChange={(e) => setHoursWorked(e.target.value)}
                            fullWidth
                        />
                    </Grid>

                    <Grid item xs={6}>
                        <Link to="/">
                            <Button type="button"
                                    variant="contained"
                                    fullWidth
                                    color="error">
                                Cancel
                            </Button>
                        </Link>
                    </Grid>
                    <Grid item xs={6}>
                        <Button type="submit"
                                variant="contained"
                                fullWidth>
                            Save Task
                        </Button>
                    </Grid>
                </Grid>

                <div className="message mt-3">{message ? <p>{message}</p> : null}</div>
            </form>
        </Container>
    );
}

export default TaskNew;
