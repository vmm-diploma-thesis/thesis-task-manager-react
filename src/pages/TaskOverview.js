import React, {useEffect, useState} from 'react';
import {Link} from "react-router-dom";
import {
    Table,
    TableCell,
    TableBody,
    TableContainer,
    TableHead,
    TableRow,
    Paper,
    Button, Container
} from '@mui/material';
import Heading from "../components/Heading";

function TaskOverview() {
    document.title = "Task Overview"
    const [error, setError] = useState(null);
    const [isLoaded, setIsLoaded] = useState(false);
    const [tasks, setTasks] = useState([]);

    useEffect(() => {
        fetch("http://localhost:8080/api/tasks")
            .then(res => res.json())
            .then(
                (result) => {
                    setIsLoaded(true);
                    setTasks(result);
                },
                (error) => {
                    setIsLoaded(true);
                    setError(error);
                }
            )
    }, [])

    if (error) {
        return <div>Error: {error.message}</div>;
    } else if (!isLoaded) {
        return <div>Loading...</div>;
    } else {
        return (
            <Container>
                <Heading text="Overview"/>

                <TableContainer component={Paper} sx={{boxShadow: 5}}>
                    <Table aria-label="task overview table">
                        <TableHead>
                            <TableRow>
                                <TableCell>ID</TableCell>
                                <TableCell>Description</TableCell>
                                <TableCell>Date done</TableCell>
                                <TableCell>Hours worked</TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {tasks.map((row) => (
                                <TableRow
                                    key={row.id}
                                    sx={{'&:last-child td, &:last-child th': {border: 0}}}>
                                    <TableCell>{row.id}</TableCell>
                                    <TableCell>{row.description}</TableCell>
                                    <TableCell>{row.dateDone}</TableCell>
                                    <TableCell>{row.hoursWorked}</TableCell>
                                </TableRow>
                            ))}
                        </TableBody>
                    </Table>
                </TableContainer>

                <div className="my-4">
                    <Link to="/new">
                        <Button variant="contained"
                                fullWidth>
                            New Task
                        </Button>
                    </Link>
                </div>
            </Container>
        );
    }
}

export default TaskOverview;
